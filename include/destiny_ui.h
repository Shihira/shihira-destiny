#ifndef  destiny_ui_INC
#define  destiny_ui_INC

#include <string>
#include <vector>

#include <QtGui>
#include "destiny_ui.uic"

class destiny_ui : public QWidget, protected Ui_destiny {
        Q_OBJECT

        std::vector<std::string> m_lines;
public:
        destiny_ui(QWidget* par = 0);

public slots:
        void go_click();
        void change_file(QString basename = "");
        void stay_top(bool is_top);
};

#endif   /* ----- #ifndef destiny_ui_INC  ----- */
