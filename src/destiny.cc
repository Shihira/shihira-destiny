#include <iostream>
#include <fstream>
#include <stdio.h>
#include <time.h>

#include "destiny_ui.h"
#include "destiny_ui.moc"

destiny_ui::destiny_ui(QWidget* par) : QWidget(par)
{
        setupUi(this);

        // Read dir lists
        QDir dir(qApp->applicationDirPath() + "/lists");
        QStringList extl; extl << "*.txt";
        QFileInfoList files = dir.entryInfoList(extl);
        for(int i = 0; i < files.size(); i++) {
                file_sel->addItem(files[i].completeBaseName());
        }

        // signal -> slot
        connect(go,             SIGNAL(clicked()),
                this,           SLOT(go_click()));
        connect(file_sel,       SIGNAL(currentIndexChanged(const QString&)),
                this,           SLOT(change_file(QString)));
        connect(check_fair,     SIGNAL(clicked()),
                this,           SLOT(change_file()));
        connect(check_top,      SIGNAL(toggled(bool)),
                this,           SLOT(stay_top(bool)));

        // Initial Settings
        change_file(file_sel->currentText());
}

void destiny_ui::stay_top(bool is_top)
{
        if(is_top) setWindowFlags(Qt::WindowStaysOnTopHint);
        else setWindowFlags(0);
        show();
}

void destiny_ui::change_file(QString basename)
{
        if(basename == "")
                basename = file_sel->currentText();
        m_lines.clear();

        // Read that file
        std::string filename = (qApp->applicationDirPath() +
                "/lists/" + basename + ".txt").toStdString();
        std::ifstream file(filename);
        while(!file.eof() && file.good()) {
                std::string line;
                std::getline(file, line, '\n');
                if(!line.empty() && check_ignore->isChecked()) // Ignore Empty Lines
                        m_lines.push_back(line);
        }
        file.close();
}

void destiny_ui::go_click()
{
        if(m_lines.empty())
                return;

        // Draw
        srand(QTime().elapsed());
        int index = rand() % m_lines.size();
        std::cout << index << " in " << m_lines.size() << std::endl;
        QTextCodec::setCodecForTr(QTextCodec::codecForLocale()); // Charset
        result->setText(tr(m_lines[index].c_str()));
        if(check_fair->isChecked()) m_lines.erase(m_lines.begin() + index); // Absolute Fairness

        if(m_lines.empty())
                change_file(file_sel->currentText());
}

int main(int argc, char* argv[])
{
        QApplication app(argc, argv);
        destiny_ui main_ui;
        main_ui.show();
        app.exec();
}

